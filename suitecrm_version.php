<?php
if (!defined('sugarEntry') || !sugarEntry) {
    die('Not A Valid Entry Point');
}

$suitecrm_version      = '7.8.0-beta';
$suitecrm_timestamp    = '2016-12-30 17:00';